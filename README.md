<p align="center">
    <img alt="J.A.R.V.I.S." src="http://i.imgur.com/AciG543.png" width="546">
</p>
<br>
<br>
<br>
[![Version](https://img.shields.io/badge/Version-1.2.5-green.svg?style=flat-square)]()
[![Status](https://img.shields.io/badge/Status-Released-green.svg?style=flat-square)]()
[![Node](https://img.shields.io/badge/Node-4.2.3-blue.svg?style=flat-square)](http://nodejs.org)
[![NPM](https://img.shields.io/badge/NPM-3.5.3-blue.svg?style=flat-square)](http://nodejs.org)
[![License](https://img.shields.io/badge/License-GPL--3.0-blue.svg?style=flat-square)]()
[![Tested on](https://img.shields.io/badge/Tested%20on-Ubuntu%2015.10-lightgrey.svg?style=flat-square)]()
<br>
<br>
J.A.R.V.I.S. is an intelligent multipurpose Discord bot.
