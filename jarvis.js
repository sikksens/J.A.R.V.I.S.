/*
 _____      ____        ____         __  __    ______      ____
/\___ \   /\  _  \     /\  _`\      /\ \/\ \  /\__  _\    /\  _`\         
\/__/\ \  \ \ \L\ \    \ \ \L\ \    \ \ \ \ \ \/_/\ \/    \ \,\L\_\       
   _\ \ \  \ \  __ \    \ \ ,  /     \ \ \ \ \   \ \ \     \/_\__ \       
  /\ \_\ \__\ \ \/\ \  __\ \ \\ \   __\ \ \_/ \__ \_\ \__  __/\ \L\ \  __ 
  \ \____/\_\\ \_\ \_\/\_\\ \_\ \_\/\_\\ `\___/\_\/\_____\/\_\ `\____\/\_\
   \/___/\/_/ \/_/\/_/\/_/ \/_/\/ /\/_/ `\/__/\/_/\/_____/\/_/\/_____/\/_/
    
    =====================================================
	  Welcome to the backend of J.A.R.V.I.S.
    It's not as neat as the man himself but it's a start!
    =====================================================
*/
var Discord = require("discord.js"),
 bot = new Discord.Client(),
 pmx = require("pmx"),
 probe = pmx.probe(),
 htmlToText = require('html-to-text'),
 request = require('request'),
 md = require('html-md'),
 usercount, channelcount, servercount, comcount, mescount, // PMX vars
 ConfigFile = require("./config.json"),
 Logger = require("./runtime/logger.js").Logger,
 Debug = require("./runtime/debugging.js"),
 ChatLogger = require("./runtime/logger.js").ChatLog,
 Commands = require("./runtime/commands.js").Commands,
 Permissions = require("./runtime/permissions.js"),
 VersionChecker = require("./runtime/versionchecker.js"),
 aliases,
 keymetrics,
 msgcount = 0,
 statuschangecount = 0,
 games = require("./runtime/games.json").games,
 Defaulting = require("./runtime/serverdefaulting.js"),
 TimeOut = require("./runtime/timingout.js"),
 Ignore = require("./runtime/ignoring.js");
const chalk = require('chalk');
 // Initial logger saying that script is being loaded.
// Should NOT be placed in init() anymore since that runs after ready
Logger.info("Loading J.A.R.V.I.S...");

// Start debug mode, if needed
Debug.prepareDebug();

// Declare aliasses
try {
  aliases = require("./runtime/alias.json");
} catch (e) {
  //No aliases defined
  aliases = {};
}

// RSS feeds
try{
var rssFeeds = require("./runtime/rss.json");
function loadFeeds(){
    for(var command in rssFeeds){
        Commands[command] = {
            usage: "[count]",
            help: rssFeeds[command].description,
            level: 0,
            url: rssFeeds[command].url,
            fn: function(bot,msg,suffix){
                var count = 1;
                if(suffix != null && suffix != "" && !isNaN(suffix)){
                    count = suffix;
                }
                rssfeed(bot,msg,this.url,count,false);
                bot.deleteMessage(msg);
            }
        };
    }
}
} catch(e) {
    console.log("Couldn't load rss.json. See rss.json.example if you want rss feed commands. error: " + e);
}

// Declare if Keymetrics analytics is needed
if (ConfigFile.bot_settings.keymetrics === true) {
  keymetrics = true;
} else {
  keymetrics = false;
}

// Error logger
bot.on("error", function(error) {
  Logger.error("Encounterd an error, please report this to the author of this bot, include any log files present in the logs folder.");
  Debug.debuglogSomething("Discord.js", "Encountered an error with discord.js most likely, got error: " + error, "error");
});

// Warning logger
bot.on('warn', function(warn) {
  Debug.debuglogSomething('Discord.js', "Encountered a Discord.js warn: " + warn, "warn");
});

// Debug stuff
bot.on('debug', function(debug) {
  Debug.debuglogSomething('Discord.js', "Debug message: " + debug, "debug");
});

// Ready announcment
bot.on("ready", function() {
	loadFeeds();
  Debug.debuglogSomething("Discord", "Ready event fired.", "info");
  if (bot.servers.length === 0) {
    Logger.warn("No servers deteted, creating default server.");
    Defaulting.create(bot);
  }
  for (var index in ConfigFile.join_on_launch) {
    bot.joinServer(ConfigFile.join_on_launch[index], function(error, server) {
      if (error) {
        Logger.warn("Couldn't join a server (" + error + ")");
      }
      if (server) {
        Logger.info("Joined " + server.name);
      }
    });
  }
  Logger.info("J.A.R.V.I.S. is Online!");
  Logger.info("Available in " + bot.channels.length + " channels");
  Logger.info("Currently serving " + bot.users.length + " users");
  bot.setPlayingGame(games[Math.floor(Math.random() * games.length)]);
  if (keymetrics === false) return;
  else {
    usercount = probe.metric({
      name: 'Users',
      value: function() {
        return bot.users.length;
      }
    });
    servercount = probe.metric({
      name: 'Servers',
      value: function() {
        return bot.servers.length;
      }
    });
    channelcount = probe.metric({
      name: 'Channels',
      value: function() {
        return bot.channels.length;
      }
    });
    comcount = probe.counter({
      name: 'Commands executed'
    });
    mescount = probe.counter({
      name: 'Messages recieved'
    });
  }
});

// Disconnected announcment
bot.on("disconnected", function() {
  Debug.debuglogSomething("Discord", "Disconnected from Discord.", "warn");
  Logger.warn("Disconnected, if this wasn't a connection issue or on purpose, report this issue to the author of the bot.");
  process.exit(0); // Disconnected announcments are not always an error, seeing that disconnections can be triggered by the user.
});

// Command checker
bot.on("message", function(msg) {
  if (keymetrics === true) mescount.inc();
  if (ConfigFile.bot_settings.log_chat === true && msg.channel.server) {
    var d = new Date();
    var n = d.toUTCString();
    ChatLogger.info(n + ": " + msg.channel.server.name + ", " + msg.channel.name + ": " + msg.author.username + " said <" + msg.cleanContent + ">");
  }
  if (msg.author.equals(bot.user)) {
    return;
  }
  if (msg.channel.isPrivate && msg.content.indexOf("https://discord.gg") === 0) { // TODO: This needs cleanup.
    bot.joinServer(msg.content, function(err, server) {
      if (err) {
        Debug.debuglogSomething("Failed to join a server on DM request.", "warn");
        bot.sendMessage(msg.author, "Something went wrong with that invite.");
      } else if (server) {
        Debug.debuglogSomething("Joined a server on DM request.", "info");
        var msgArray = [];
        msgArray.push("Good Day, I'm **" + bot.user.username + "**, " + msg.author + " invited me to this server.");
        msgArray.push("If I'm intended to be in this server, you may use **" + ConfigFile.bot_settings.cmd_prefix + "help** to see what I can do!");
        msgArray.push("If you don't want me here, you may use **" + ConfigFile.bot_settings.cmd_prefix + "leave** to ask me to leave.");
        Permissions.SetLevel((server.id + server.owner.id), 4, function(err, level) {
          if (err) {
            msgArray.push("An error occured while auto-setting " + server.owner + " to level 4, try running `setowner` a bit later.");
          }
          if (level === 4) {
            msgArray.push("I have detected " + server.owner + " as the server owner and made him/her an admin over me.");
          }
        });
        bot.sendMessage(server.defaultChannel, msgArray);
        msgArray = [];
        msgArray.push("Hey " + server.owner.username + ", I've joined " + server.name + " in which you're the founder.");
        msgArray.push("I'm " + bot.user.username + " by the way, a Discord bot, meaning that all of the things I do are mostly automated.");
        msgArray.push("If you are not keen on having me in your server, you may use `" + ConfigFile.bot_settings.cmd_prefix + "leave` in the server I'm not welcome in.");
        msgArray.push("If you do want me, use `" + ConfigFile.bot_settings.cmd_prefix + "help` to see what I can do.");
        bot.sendMessage(server.owner, msgArray);
        bot.sendMessage(msg.channel, "I've successfully joined **" + server.name + "**");
      }
    });
  }

  var flips = msg.content.match(/(\(╯°□°\）╯︵ ┻━┻)/g);
   {
     if(flips != null)
     {
       var str = "";
       flips.forEach(function(){
         str += ("┬─┬﻿ ノ( ゜-゜ノ)\t");
       });
       bot.sendMessage(msg.channel, str);
     }
   }
   if(msg.content.match("kappa")) {
                    var kappaArray = ["./images/emotes/kappaross.jpg", "./images/emotes/kappa.png", "./images/emotes/kappajoker.png", "./images/emotes/kappaHD.png"];
                    bot.sendFile(msg.channel, kappaArray[Math.floor(Math.random() * kappaArray.length)])
                }
            if(msg.content.match("rage")) {
                    var rageArray = ["./images/emotes/rage.png", "./images/emotes/rage1.png", "./images/emotes/rage2.png", "./images/emotes/rage3.png"];
                    bot.sendFile(msg.channel, rageArray[Math.floor(Math.random() * rageArray.length)])
                }
            if(msg.content.match("hype")) {
                    var hypeArray = ["./images/emotes/hype.png", "./images/emotes/hype2.png"];
                    bot.sendFile(msg.channel, hypeArray[Math.floor(Math.random() * hypeArray.length)])
            }
            if(msg.content.match("rekt")) {
                    var rektArray = ["./images/emotes/rekt.png", "./images/emotes/rekt2.png", "./images/emotes/rekt3.png"];
                    bot.sendFile(msg.channel, rektArray[Math.floor(Math.random() * rektArray.length)])
            }
            if(msg.content.match("troll")) {
                    var trollArray = ["./images/emotes/troll.png"];
                    bot.sendFile(msg.channel, trollArray[Math.floor(Math.random() * trollArray.length)])
            }
            if(msg.content.match("ttime")) {
                    var ttimeArray = ["./images/emotes/ttime.png", "./images/emotes/ttime2.png"];
                    bot.sendFile(msg.channel, ttimeArray[Math.floor(Math.random() * ttimeArray.length)])
            }
            if(msg.content.match("o/")) {
                    var wavesArray = ["./images/emotes/waves.png"];
                    bot.sendFile(msg.channel, wavesArray[Math.floor(Math.random() * wavesArray.length)])
            }
            if(msg.content.match("o7")) {
                    var saluteArray = ["./images/emotes/salute.png"];
                    bot.sendFile(msg.channel, saluteArray[Math.floor(Math.random() * saluteArray.length)])
            }
            if(msg.content.match("facepalm")) {
                    var facepalmArray = ["./images/emotes/facepalm.png"];
                    bot.sendFile(msg.channel, facepalmArray[Math.floor(Math.random() * facepalmArray.length)])
            }
            if(msg.content.match("shrug")) {
                    var shrugArray = ["¯\\\\\\_(ツ)_/¯"];
                    bot.sendMessage(msg.channel, shrugArray[Math.floor(Math.random() * shrugArray.length)])
            }

            if(msg.content.indexOf("feelsbad") > -1){
                bot.sendFile(msg.channel, "./images/emotes/feelsbad.png")
                }
            if(msg.content.indexOf("feelsgood") > -1){
                bot.sendFile(msg.channel, "./images/emotes/feelsgood.png")
                }
            if(msg.content.indexOf("cry") > -1){
                bot.sendFile(msg.channel, "./images/emotes/biblethump.png")
                }
            if(msg.content.indexOf("legion") > -1){
                bot.sendFile(msg.channel, "./images/emotes/kappalegion.jpg")
                }
        if(msg.content.indexOf("treason") > -1) {
                bot.sendMessage(msg, 'http://38.media.tumblr.com/57ecda4db728e5279da8b34683161916/tumblr_njskklyhWV1tiwa1fo1_500.gif');
        }
        if(msg.content.startsWith("hello") || msg.content.startsWith("sup") || msg.content.startsWith("hey") || msg.content.startsWith("howzit")) {
                    var greetingArray = ["Good Day, Sir!", "Hello Sir", "Hello Sir, how are you this fine day?", "Good to see you sir, Welcome!"];
                    bot.reply(msg, greetingArray[Math.floor(Math.random() * greetingArray.length)])
        }

  var prefix;
  if (ConfigFile.bot_settings.cmd_prefix != "mention") {
    prefix = ConfigFile.bot_settings.cmd_prefix;
  } else if (ConfigFile.bot_settings.cmd_prefix === "mention") {
    prefix = bot.user + " ";
  } else {
    Debug.debuglogSomething("Weird prefix detected.", "warn");
  }
  if (msg.content.indexOf(prefix) === 0) {
    if (keymetrics === true) comcount.inc();
    Logger.info("Executing <" + msg.cleanContent + "> from " + msg.author.username);
    var step = msg.content.substr(prefix.length);
    var chunks = step.split(" ");
    var command = chunks[0];
    alias = aliases[command];
    var suffix = msg.content.substring(command.length + (prefix.length + 1));
    if (alias) {
      command = alias[0];
      suffix = alias[1] + " " + suffix;
    }
    if (command === "help") {
      Commands.help.fn(bot, msg, suffix);
      return;
    }
    if (Commands[command]) {
      Debug.debuglogSomething("Command detected, trying to execute.", "info");
      if (Commands[command].music && msg.channel.server) {
        Debug.debuglogSomething("Musical command detected, checking for user role.", "info");
        DJ.checkPerms(msg.channel.server, msg.author, function(err, reply) {
          if (reply === 1 && !err) {
            Commands[command].fn(bot, msg, suffix);
          } else if (reply === 0 && !err) {
            bot.reply(msg, "you need a role called `Radio Master` to use music related commands, even if you're the server owner.");
            return;
          } else if (err) {
            bot.sendMessage(msg.channel, "Something went wrong, try again later.");
            return;
          }
        });
      } else if (Commands[command].music && !msg.channel.server) {
        Debug.debuglogSomething("Musical command detected, but was excuted in a DM.", "info");
        bot.sendMessage(msg.channel, "You cannot use music commands in a DM, dummy!");
        return;
      }
      if (msg.channel.server && !Commands[command].music) {
        Permissions.GetLevel((msg.channel.server.id + msg.author.id), msg.author.id, function(err, level) {
          if (err) {
            Debug.debuglogSomething("LevelDB", "GetLevel failed, got error: " + err, "error");
            Logger.debug("An error occured!");
            return;
          }
          if (level >= Commands[command].level) {
            Debug.debuglogSomething("Execution of command allowed.", "info");
            if (Commands[command].timeout) {
              TimeOut.timeoutCheck(command, msg.channel.server.id, function(reply) {
                if (reply === "yes") {
                  Debug.debuglogSomething("Command is on cooldown, execution halted.", "info");
                  bot.sendMessage(msg.channel, "Sorry, this command is on cooldown.");
                  return;
                }
              });
            }
            if (!Commands[command].nsfw) {
              Debug.debuglogSomething("Safe for work command executed.", "info");
              Commands[command].fn(bot, msg, suffix);
              if (Commands[command].timeout) {
                TimeOut.timeoutSet(command, msg.channel.server.id, Commands[command].timeout, function(reply, err) {
                  if (err) {
                    Logger.error("Resetting timeout failed!");
                  }
                });
              }
              return;
            } else {
              Permissions.GetNSFW(msg.channel, function(err, reply) {
                Debug.debuglogSomething("Command is NSFW, checking if channel allows that.", "info");
                if (err) {
                  Logger.debug("Got an error! <" + err + ">");
                  Debug.debuglogSomething("LevelDB", "NSFW channel check failed, got error: " + err, "error");
                  bot.sendMessage(msg.channel, "Sorry, an error occured, try again later.");
                  return;
                }
                if (reply === "on") {
                  Debug.debuglogSomething("NSFW command successfully executed.", "info");
                  Commands[command].fn(bot, msg, suffix);
                  return;
                } else {
                  Debug.debuglogSomething("NSFW command execution failed because of channel settings.", "info");
                  bot.sendMessage(msg.channel, "You cannot use NSFW commands in this channel!");
                }
              });
            }
          } else {
            Debug.debuglogSomething("User has no permission to use that command.", "info");
            bot.sendMessage(msg.channel, "You don't have permission to use this command!");
            return;
          }
        });
      } else if (!msg.channel.server) {
        Permissions.GetLevel(0, msg.author.id, function(err, level) { // Value of 0 is acting as a placeholder, because in DM's only global permissions apply.
          Debug.debuglogSomething("DM command detected, getting global perms.", "info");
          if (err) {
            Logger.debug("An error occured!");
            Debug.debuglogSomething("LevelDB", "GetLevel failed, got error: " + err, "error");
            return;
          }
          if (level >= Commands[command].level) {
            Debug.debuglogSomething("User has sufficient global perms.", "info");
            Commands[command].fn(bot, msg, suffix);
            return;
          } else {
            Debug.debuglogSomething("User does not have enough global permissions.", "info");
            bot.sendMessage(msg.channel, "Only global permissions apply in DM's, your server specific permissions do nothing here!");
          }
        });
      }
    }
  }
});

// Initial functions
function init(token) {
  Debug.debuglogSomething("Discord", "Sucessfully logged into Discord, returned token: " + token, "info");
  Debug.debuglogSomething("Continuing start-up sequence.", "info");
  Logger.info("Checking for updates...");
  VersionChecker.getStatus(function(err, status) {
    if (err) {
      Logger.error(err);
      Debug.debuglogSomething("VersionChecker", "Version checking failed, got error: " + err, "error");
    } // error handle
    if (status && status !== "failed") {
      Logger.info(status);
    }
  });
}

var rssgames = {
    "mhpatch": "Marvel Heroes 2016",
    "gw2": "Guild Wars 2",
    "d3": "Diablo 3",
    "wow": "World of Warcraft",
    "ps2alert": "Planetside 2"
};

function rssfeed(bot, msg, url, count, full) {
  var FeedParser = require('feedparser');
  var feedparser = new FeedParser();
  var request = require('request');
  request(url).pipe(feedparser);
  feedparser.on('error', function(error) {
    bot.sendMessage(msg.channel, "failed reading feed: " + error);
  });
  var game = msg.content.substr(1).trim();
  if(rssgames[game]) {
	  		game = rssgames[game];
	  	}
  var shown = 0;
  feedparser.on('readable', function() {
    var stream = this;
    shown += 1;
    if (shown > count) {
      return;
    }
    var item = stream.read();
    bot.sendMessage(msg.channel, "**" + game + "**\n***" + item.title + "***\n" + md(item.summary) + "\n" + item.link, function() {
      if (full === true) {
        var text = htmlToText.fromString(item.description, {
          wordwrap: false,
          ignoreHref: true
        });
        bot.sendMessage(msg.channel, text);
      }
    });
    stream.alreadyRead = true;
  });
}

function getCmd(cmd, index) {
    // Used for getting commands at split[index]
    var i = index == undefined ? 0 : index;
    return cmd.split(' ')[i];
}

function splitCmd(cmd, index) {
    // Index is used for telling how many splits to skip
    var i = index === undefined ? 1 : index;
    var split = cmd.split(' ');	
    var joined = '';
    for (var i = index; i < split.length; i++) {
        joined += split[i] + ' ';
    }
    return joined;
}

setInterval(function() {
	bot.setPlayingGame(games[Math.floor(Math.random() * (games.length))]);
}, 3600000); //change playing game every 60 minutes

// New user welcomer
bot.on("serverNewMember", function(server, user) {
  if (ConfigFile.bot_settings.welcome_new_members === false) return;
  bot.sendMessage(server.defaultChannel, "Welcome **" + user.username + " :smiley:" + "** - type **\\\!help\** for a list of my commands!");
});

bot.on("presence", (userOld, user) => { //check if game and also it's now oldUser newUser
		if (user.game === null || user.game === undefined) { console.log(chalk.blue.bold(`[` + new Date().toLocaleTimeString('en-GB', ConfigFile.timeFormat) + `]`+user.username + " is now " + user.status));
		} else { console.log(chalk.blue.bold(`[` + new Date().toLocaleTimeString('en-GB', ConfigFile.timeFormat) + `]`+user.username + " is playing " + user.game.name)); }
	}
);

function err(error) {
  Debug.debuglogSomething("Discord", "Logging into Discord probably failed, got error: " + error, "error");
}

process.on('uncaughtException', function(err) {

  if (err.code == 'ECONNRESET') {
    console.log(chalk.red.bold('Got an ECONNRESET! This is *probably* not an error. Stacktrace:'));
    console.log(err.stack);
  } else {
    // Normal error handling
    console.log(err);
    console.log(err.stack);
    process.exit(0);
  }
});
// Connection starter
bot.login(ConfigFile.discord.email, ConfigFile.discord.password).then(init).catch(err);