var ConfigFile = require("../config.json"),
  Logger = require("./logger.js").Logger,
  Permissions = require("./permissions.js"),
  Giphy = require("./giphy.js"),
  os = require('os-utils'),
  Cleverbot = require('cleverbot-node'),
  cleverbot = new Cleverbot(),
  request = require('request'),
  $ = require('cheerio'),
  gs = require("./google_plugin"),
  google_plugin = new gs(),
  yt = require("./youtube_plugin"),
  youtube_plugin = new yt(),
  wa = require("./wolfram_plugin"),
  wolfram_plugin = new wa(),
  version = require("../package.json").version,
  unirest = require('unirest'),
  Debug = require("./debugging.js"),
  Defaulting = require("./serverdefaulting.js"),
  game_abbreviations = require("./abbreviations.json"),
  DJ = require("./djlogic.js"),
  aliases = require("./alias.json"),
  ignore = require("./ignoring.js"),
  help = require("./help.json"),
  http = require('http'),
  getos = require('getos'),
  msgcount = 0,
  statuschangecount = 0,
  memuse = function() { return(Math.round(process.memoryUsage().rss / 1024 / 1000 * 100 ) / 100)};
  


var Commands = [];

Commands.ping = {
  name: "ping",
  help: "I'll reply to you with pong!",
  level: 0,
  timeout: 10,
  fn: function(bot, msg) {
    bot.sendMessage(msg.channel, "Pong!");
  }
};

Commands.eval = {
  name: "eval",
  help: "Allows the execution of arbitrary Javascript code within the context of the bot.",
  level: 6, // Now 100% sure it can't be used by anyone but the master user.
  fn: function(bot, msg, suffix) {
    try {
      bot.sendMessage(msg.channel, eval(suffix));
    } catch (err) {
      bot.sendMessage(msg.channel, "Eval failed :(");
      bot.sendMessage(msg.channel, "```" + err + "```");
    }
  }
};

Commands.alias = {
  name: "alias",
  help: "Allows for creating quick custom commands on the fly!",
  level: 5,
  fn: function(bot, msg, suffix) {
    var args = suffix.split(" ");
    var name = args.shift();
    if (!name) {
      return;
    } else if (Commands[name] || name === "help") {
      bot.sendMessage(msg.channel, "Overwriting commands with aliases is not allowed!");
    } else {
      var command = args.shift();
      aliases[name] = [command, args.join(" ")];
      //now save the new alias
      require("fs").writeFile("./runtime/alias.json", JSON.stringify(aliases, null, 2), null);
      bot.sendMessage(msg.channel, "Created alias " + name);
    }
  }
};

Commands.fortunecow = {
  name: "fortunecow",
  help: "I'll get a random fortunecow!",
  level: 0,
  fn: function(bot, msg) {
    bot.startTyping(msg.channel);
    unirest.get("https://thibaultcha-fortunecow-v1.p.mashape.com/random")
      .header("X-Mashape-Key", ConfigFile.api_keys.mashape_key)
      .header("Accept", "text/plain")
      .end(function(result) {
        bot.sendMessage(msg.channel, "```" + result.body + "```");
        bot.deleteMessage(msg);
        bot.stopTyping(msg.channel);
      });
  }
};

Commands.leetspeak = {
  name: "leetspeak",
  help: "1'Ll 3nc0D3 Y0uR Me5s@g3 1Nt0 l337sp3@K!",
  level: 0,
  fn: function(bot, msg, suffix) {
    var leetspeak = require("leetspeak");
    var thing = leetspeak(suffix);
    bot.sendMessage(msg.channel, thing);
  }
};

Commands.cat = {
  name: "cat",
  help: "I'll get a random cat image for you!",
  level: 0,
  fn: function(bot, msg, suffix) {
    bot.startTyping(msg.channel);
    request("http://random.cat/meow", function(error, response, body) {
            if(!error && response.statusCode == 200) {
                var result = JSON.parse(body);
                bot.sendMessage(msg, result.file);
                bot.deleteMessage(msg);
                bot.stopTyping(msg.channel);
            }
      });
  }
};

Commands.joke = {
  name: "joke",
  help: "I will tell you a random joke",
  level: 0,
  fn: function(bot, msg, suffix) {
            http.get("http://tambal.azurewebsites.net/joke/random", (res) => {
                res.on('data', (d) => {
                    bot.sendMessage(msg.channel, JSON.parse(d).joke);
                    bot.deleteMessage(msg);
                });
            }).on('error', (e) => {
                console.error(e);
              });
          }
};

Commands.quote = {
  name: "quote",
  help: "I will post a random quote",
  level: 0,
  fn: function(bot, msg) {
            var Quotes = require("./quotes.json");
            var rand = Math.floor(Math.random() * Quotes.length);
            bot.sendMessage(msg.channel, Quotes[rand]);
            bot.deleteMessage(msg);
        }
};

Commands.convert = {
        name: "convert",
        help: "A convertion tool used to convert just about anything.",
        usage: "ex: 5 days in hours",
        level: 0,
        fn: function(bot,msg,suffix){
          var units = require("node-units");
            try{
                var value = units.convert(suffix);
                bot.reply(msg, value);
            } catch(e){
                bot.reply(msg, e);
                bot.deleteMessage(msg);
            }
        }
        
    };

Commands.status = {
  name: "status",
  help: "I'll print some information about me.",
  level: 0,
  fn: function(bot, msg) {
    getos(function(e,os) {
      JSON.stringify(os)
      if(e) return console.log(e)
    var lmsg = []
           lmsg.push("```")
           lmsg.push("Online!")
           lmsg.push("Version: " +version)
           lmsg.push("Library: Discord.js 6.0.0")
           lmsg.push("-----------------")
           lmsg.push("Serving " + bot.users.length + " users")
           lmsg.push("Currently in " + bot.channels.length + " channels")
           lmsg.push("Status changes logged: " + statuschangecount)
           lmsg.push("Messages logged: " + msgcount)
           lmsg.push("-----------------")
           lmsg.push("Running On: " + os.dist+" "+os.release+"("+os.codename+")")
           lmsg.push("Using " + memuse() + "MB of RAM")
           lmsg.push(Object.keys(Commands).length + " plugins currently running")
           lmsg.push("Uptime: " + (Math.round(bot.uptime / (1000 * 60 * 60))) + " hours, " + (Math.round(bot.uptime / (1000 * 60)) % 60) + " minutes, and " + (Math.round(bot.uptime / 1000) % 60) + " seconds.")
           lmsg.push("```")
           bot.sendMessage(msg.channel, lmsg);
           bot.deleteMessage(msg);
    })
  }
};

Commands.chat = {
  name: "chat",
  help: "I'll act as Cleverbot when you execute this command, remember to enter a message as suffix.",
  usage: "<message>",
  level: 0,
  fn: function(bot, msg, suffix) {
    Cleverbot.prepare(function() {
      bot.startTyping(msg.channel);
      cleverbot.write(suffix, function(response) {
        bot.sendMessage(msg.channel, response.message);
        bot.stopTyping(msg.channel);
      });
    });
  }
};

Commands.say = {
  name: "say",
  help: "I'll echo the suffix of the command to the channel and, if I have sufficient permissions, deletes the command.",
  usage: "<text>",
  level: 0,
  fn: function(bot, msg, suffix) {
    if (suffix.search("!say") === -1) {
      bot.sendMessage(msg.channel, suffix);
      if (msg.channel.server) {
        var bot_permissions = msg.channel.permissionsOf(bot.user);
        if (bot_permissions.hasPermission("manageMessages")) {
          bot.deleteMessage(msg);
          return;
        } else {
          bot.sendMessage(msg.channel, "*This works best when I have the permission to delete messages!*");
        }
      }
    } else {
      bot.sendMessage(msg.channel, "HEY " + msg.sender + " STOP THAT!", {
        tts: "true"
      });
    }
  }
};

Commands.dismiss = {
  name: "dismiss",
  help: "Gives J.A.R.V.I.S. the day off.",
  level: 6, // If an access level is set to 4 or higher, only the master user can use this
  fn: function(bot, msg) {
      bot.logout();
      Logger.log("warn", "Disconnected via dismiss");
      process.exit(0);
    } //exit node.js without an error
};

Commands.image = {
  name: "image",
  help: "I'll search teh interwebz for a picture matching your tags.",
  usage: "<image tags>",
  level: 0,
  fn: function(bot, msg, suffix) {
    if (!ConfigFile || !ConfigFile.api_keys.google_key || !ConfigFile.api_keys.cse_key) {
      bot.sendMessage(msg.channel, "Image search requires **both** a Google API key and a CSE key!");
      return;
    }
    //gets us a random result in first 5 pages
    var page = 1 + Math.floor(Math.random() * 5) * 10; //we request 10 items
    var request = require("request");
    request("https://www.googleapis.com/customsearch/v1?key=" + ConfigFile.api_keys.google_key + "&cx=" + ConfigFile.api_keys.cse_key + "&q=" + (suffix.replace(/\s/g, '+')) + "&searchType=image&alt=json&num=10&start=" + page, function(err, res, body) {
      var data, error;
      try {
        data = JSON.parse(body);
      } catch (error) {
        Logger.error(error);
        return;
      }
      if (!data) {
        Logger.debug(data);
        bot.sendMessage(msg.channel, "Error:\n" + JSON.stringify(data));
        return;
      } else if (!data.items || data.items.length === 0) {
        Logger.debug(data);
        bot.sendMessage(msg.channel, "No result for '" + suffix + "'");
        return;
      }
      var randResult = data.items[Math.floor(Math.random() * data.items.length)];
      bot.sendMessage(msg.channel, randResult.title + '\n' + randResult.link);
      bot.deleteMessage(msg);
    });
    Logger.log("debug", "I've looked for images of " + suffix + " for " + msg.sender.username);
  }
};

Commands.pullanddeploy = {
  name: "pullanddeploy",
  help: "I'll check if my code is up-to-date via GitLab, and restart.",
  level: 5, // If an access level is set to 4 or higher, only the master user can use this
  fn: function(bot, msg, suffix) {
    bot.sendMessage(msg.channel, "Fetching updates...", function(error, sentMsg) {
      Logger.log("info", "Updating...");
      var spawn = require('child_process').spawn;
      var log = function(err, stdout, stderr) {
        if (stdout) {
          Logger.log("debug", stdout);
        }
        if (stderr) {
          Logger.log("debug", stderr);
        }
      };
      var fetch = spawn('git', ['fetch']);
      fetch.stdout.on('data', function(data) {
        Logger("debug", data.toString());
      });
      fetch.on("close", function(code) {
        var reset = spawn('git', ['reset', '--hard', 'origin/master']);
        reset.stdout.on('data', function(data) {
          Logger.log("debug", data.toString());
        });
        reset.on("close", function(code) {
          var npm = spawn('npm', ['install']);
          npm.stdout.on('data', function(data) {
            Logger.log("debug", data.toString());
          });
          npm.on("close", function(code) {
            Logger.log("info", "Goodbye");
            bot.sendMessage(msg.channel, "brb!", function() {
              bot.logout(function() {
                process.exit();
              });
            });
          });
        });
      });
    });
  }
};

Commands.google = {
  name: "google",
  help: "I'll search Google for your request.",
  usage: "<google search>",
  level: 0,
        description: "gets first result from google",
        fn: function(bot,msg,suffix) {
            google_plugin.respond(suffix,msg.channel,bot);
            bot.deleteMessage(msg);
            }
     };

Commands.youtube = {
  name: "youtube",
  help: "I'll search YouTube for a video matching your given tags.",
  usage: "<video tags>",
  level: 0,
  fn: function(bot, msg, suffix) {
    youtube_plugin.respond(suffix, msg.channel, bot);
    bot.deleteMessage(msg);
  }
};

Commands.wolfram = {
    name: "wolfram",
    help: "I'll search Wolfram Alpha for your request.",
		usage: "<search terms>",
		level: 0,
    fn: function(bot, msg, suffix) {
			if(!suffix){
				bot.sendMessage(msg.channel,"Usage: !wolfram <search terms> (Ex. !wolfram integrate 4x)");
			}
            wolfram_plugin.respond(suffix,msg.channel,bot);
        }
	};
	
Commands.wiki = {
    name: "wiki",
    help: "returns the summary of the first matching search result from Wikipedia",
    usage: "<search terms>",
    level: 0,
      fn: function(bot,msg,suffix) {
      var query = suffix;
      if(!query) {
      bot.sendMessage(msg.channel,"usage: !wiki search terms");
      return;
      }
      var Wiki = require('wikijs');
      new Wiki().search(query,1).then(function(data) {
      new Wiki().page(data.results[0]).then(function(page) {
      page.summary().then(function(summary) {
      var sumText = summary.toString().split('\n');
      var continuation = function() {
      var paragraph = sumText.shift();
      if(paragraph){
      bot.sendMessage(msg.channel, paragraph);
      bot.deleteMessage(msg);
                            }
                        };
                        continuation();
                    });
                });
            },function(err){
                bot.sendMessage(msg.channel,err);
            });
        }
    };
    
Commands.weather = {
         name: "weather",
         help: "Use the city name or post code, country codes are optional and based off: https://en.wikipedia.org/wiki/ISO_3166-1_alpha-2",
         usage: "<city/postcode;country>",
         level: 0,
         fn: function(bot,msg,suffix){ 
           bot.startTyping(msg.channel);
 		var weatherurl = "http://api.openweathermap.org/data/2.5/weather?q=" + suffix + "&appid=d4d04958f326e7f14199d39fdde7de1c";
 		request(weatherurl, function (error, response, html) {
 			if (!error && response.statusCode == 200 && suffix != null) {
 				var weathersearch = JSON.parse(html);
 				var conditions = weathersearch.weather[0].main;
 				var tempC = (weathersearch.main.temp-273.15).toFixed(2);
 				var tempF = ((weathersearch.main.temp*9/5)-459.67).toFixed(2);
 				if(weathersearch.wind.deg > 337.5 || weathersearch.wind.deg <22.5) {
 					var windDir = "North";
 				} else if (weathersearch.wind.deg > 22.5 && weathersearch.wind.deg <67.5) {
 					var windDir = "North East";
 				} else if (weathersearch.wind.deg > 67.5 && weathersearch.wind.deg <112.5) {
 					var windDir = "East";
 				} else if (weathersearch.wind.deg > 112.5 && weathersearch.wind.deg <157.5) {
 					var windDir = "South East";
 				} else if (weathersearch.wind.deg > 157.5 && weathersearch.wind.deg <202.5) {
 					var windDir = "South";
 				} else if (weathersearch.wind.deg > 202.5 && weathersearch.wind.deg <247.5) {
 					var windDir = "South West";
 				} else if (weathersearch.wind.deg > 247.5 && weathersearch.wind.deg <292.5) {
 					var windDir = "West";
 				} else {
 					var windDir = "North West";
 				};
 				var windSpdK = (weathersearch.wind.speed*18/5).toFixed(2);
 				var windSpdM = (weathersearch.wind.speed*2.237).toFixed(2);
 				var humidity = weathersearch.main.humidity;
 				var city = weathersearch.name;
 				var place = weathersearch.sys.country;
 				var windchillF = (35.74+(0.6215*tempF)-(35.75*windSpdM*Math.pow(10,0.16))+(0.4275*tempF*windSpdM*Math.pow(10,0.16))).toFixed(2);
 				var windchillC = ((windchillF-32)*5/9).toFixed(2)
 				var sunrise = new Date(weathersearch.sys.sunrise*1000)
				var formattedSunrise = (sunrise.getHours()) + ':' + ("0" + sunrise.getMinutes()).substr(-2)
				var sunset = new Date(weathersearch.sys.sunset*1000)
				var formattedSunset = (sunset.getHours()) + ':' + ("0" + sunset.getMinutes()).substr(-2)
 				var emoji = "☀";
					if (weathersearch.weather[0].description.indexOf("cloud") > -1 || weathersearch.weather[0].description.indexOf("mist") > -1) { emoji = "☁"; }
					if (weathersearch.weather[0].description.indexOf("snow") > -1) { emoji = "❄"; }
					if (weathersearch.weather[0].description.indexOf("rain") > -1 || weathersearch.weather[0].description.indexOf("storm") > -1 || weathersearch.weather[0].description.indexOf("drizzle") > -1) { emoji = "☔"; }
 				bot.sendMessage(msg.channel,"The weather right now in " + "**"+city+", "+place+"**" + " is: "+ 
 				"\n" + emoji + "** Conditions:** " + conditions + " / " + tempC + "°C" +
 				"\n" + "💨 **Wind:** " + windSpdK + "kph" + " blowing "+ windDir +
 				"\n" + "😓 **Humidity:** " + humidity + "%"+
 				"\n" + "**🌅 Sunrise:** "+formattedSunrise+" UTC  **🌇 Sunset:** "+formattedSunset+" UTC");
 				bot.deleteMessage(msg);
 				bot.stopTyping(msg.channel);
 			} else {
 				bot.sendMessage(msg.channel,"Please input a city/zip and country using the syntax <city/postcode;country>")
 			}
 		});
 		}
    };
    
Commands.twitch = {
    name: "twitch",
    help: "checks if the given stream is online",
		usage: "<streamer-name>",
		level: 0,
		fn: function(bot,msg,suffix){
			request("https://api.twitch.tv/kraken/streams/"+suffix,
			function(err,res,body){
				var stream = JSON.parse(body);
				if(stream.stream){
					bot.sendMessage(msg.channel, ":green_book: "+"**"+stream.stream.channel.display_name+"**"+" is **online!**"
						+"\n **Playing:** "+stream.stream.game
						+"\n **Title:** "+stream.stream.channel.status
						+"\n **Viewers:** "+stream.stream.viewers
						+"\nhttp://twitch.tv/"+suffix)
				}else{
					bot.sendMessage(msg.channel, ":red_circle: "+"**"+suffix+"**"+" is offline")
					bot.deleteMessage(msg);
				}
			});
		}
	};
	
Commands.rss = {
        name: "Rss",
        help: "lists available rss feeds",
        level: 0,
        fn: function(bot,msg,suffix) {
            /*var args = suffix.split(" ");
            var count = args.shift();
            var url = args.join(" ");
            rssfeed(bot,msg,url,count,full);*/
            bot.sendMessage(msg.author,"Available feeds:", function(){
                for(var c in rssFeeds){
                    bot.sendMessage(msg.author,c + ": " + rssFeeds[c].url);
                }
            });
        }
    };

Commands.purge = {
  name: "purge",
  help: "I'll delete a certain ammount of messages.",
  usage: "<number-of-messages-to-delete>",
  level: 2,
  fn: function(bot, msg, suffix) {
    if (!msg.channel.server) {
      bot.sendMessage(msg.channel, "You can't do that in a DM, sir!");
      return;
    }
    if (!suffix || isNaN(suffix)) {
      bot.sendMessage(msg.channel, "Please define an ammount of messages for me to delete!");
      return;
    }
    if (!msg.channel.permissionsOf(msg.sender).hasPermission("manageMessages")) {
      bot.sendMessage(msg.channel, "Sorry, your role in this server does not have enough permissions.");
      return;
    }
    if (!msg.channel.permissionsOf(bot.user).hasPermission("manageMessages")) {
      bot.sendMessage(msg.channel, "I don't have permission to do that!");
      return;
    }
    if (suffix > 100) {
      bot.sendMessage(msg.channel, "The maximum is 100.");
      return;
    }
    bot.getChannelLogs(msg.channel, suffix, function(error, messages) {
      if (error) {
        bot.sendMessage(msg.channel, "Something went wrong while fetching logs.");
        return;
      } else {
        Logger.info("Beginning purge...");
        var todo = messages.length,
          delcount = 0;
        for (msg of messages) {
          bot.deleteMessage(msg);
          todo--;
          delcount++;
          if (todo === 0) {
            bot.sendMessage(msg.channel, "✅ Done! Deleted " + delcount + " messages.");
            Logger.info("Ending purge, deleted " + delcount + " messages.");
            return;
          }
        }
      }
    });
  }
};

Commands.whois = {
  name: "whois",
  help: "I'll get some information about the user you've mentioned.",
  level: 0,
  fn: function(bot, msg) {
    var UserLevel = 0;
    if (!msg.channel.server) {
      bot.sendMessage(msg.author, "I can't do that in a DM, sorry.");
      return;
    }
    if (msg.mentions.length === 0) {
      bot.sendMessage(msg.channel, "⚠ Please mention the user that you want to get information of.");
      return;
    }
    msg.mentions.map(function(user) {
      Permissions.GetLevel((msg.channel.server.id + user.id), user.id, function(err, level) {
        if (err) {
          return;
        } else {
          UserLevel = level;
        }
        var msgArray = [];
        if (user.avatarURL === null) {
          msgArray.push("Requested user: `" + user.username + "`");
          msgArray.push("Status: `" + user.status + "`");
          msgArray.push("ID: `" + user.id + "`");
          var jDate = new Date(msg.channel.server.detailsOfUser(user).joinedAt);
          msgArray.push("Joined on: `" + jDate.toUTCString()+ "`");
          bot.sendMessage(msg.channel, msgArray);
          return;
        } else {
          msgArray.push("Requested user: `" + user.username + "`");
          msgArray.push("Status: `" + user.status + "`");
          msgArray.push("ID: `" + user.id + "`");
          var jDate = new Date(msg.channel.server.detailsOfUser(user).joinedAt);
          msgArray.push("Joined on: `" + jDate.toUTCString()+ "`");
          msgArray.push("Avatar: " + user.avatarURL);
          bot.sendMessage(msg.channel, msgArray);
          bot.deleteMessage(msg);
        }
      });
    });
  }
};

Commands.remindme = {
         help: "Set a reminder in minutes",
         usage: "<time(in minutes)> <message>",
         level: 0,
         fn: function(bot, msg) {
            var opt = getCmd(msg.content, 1);
            if (opt == parseInt(opt)) {
                    opt = parseInt(opt);
                    var txt = splitCmd(msg.content, 2);
                    bot.sendMessage(msg, 'I will remind you in ' + opt + ' minutes.');
                    bot.deleteMessage(msg);
                    opt = opt * 1000 * 60; // milliseconds to minutes
                    setTimeout(function() {
                        bot.sendMessage(msg, msg.sender.mention() + ' ' + txt);
                    }, opt);
            }
        }
    }

Commands.setlevel = {
  name: "setlevel",
  help: "This changes the permission level of an user.",
  level: 6,
  fn: function(bot, msg, suffix) {
    if (!msg.channel.server) {
      bot.sendMessage(msg.channel, "I can't do that in a PM!");
      return;
    }
    if (isNaN(suffix[0])) {
      bot.reply(msg.channel, "your first param is not a number!");
      return;
    }
    if (suffix[0] > 3) {
      bot.sendMessage(msg.channel, "Setting a level higher than 3 is not allowed.");
      return;
    }
    if (msg.mentions.length === 0) {
      bot.reply(msg.channel, "please mention the user(s) you want to set the permission level of.");
      return;
    }
    Permissions.GetLevel((msg.channel.server.id + msg.author.id), msg.author.id, function(err, level) {
      if (err) {
        bot.sendMessage(msg.channel, "Help! Something went wrong!");
        return;
      }
      if (suffix[0] > level) {
        bot.reply(msg.channel, "you can't set a user's permissions higher than your own!");
        return;
      }
    });
    msg.mentions.map(function(user) {
      Permissions.SetLevel((msg.channel.server.id + user.id), suffix[0], function(err, level) {
        if (err) {
          bot.sendMessage(msg.channel, "Help! Something went wrong!");
          return;
        }
      });
    });
    bot.sendMessage(msg.channel, "Alright! The permission levels have been set successfully!");
  }
};

Commands.setnsfw = {
  name: "setnsfw",
  help: "This changes if the channel allows NSFW commands.",
  usage: "<on | off>",
  level: 6,
  fn: function(bot, msg, suffix) {
    if (!msg.channel.server) {
      bot.sendMessage(msg.channel, "NSFW commands are always allowed in DM's.");
      return;
    }
    if (suffix === "on" || suffix === "off") {
      Permissions.SetNSFW(msg.channel, suffix, function(err, allow) {
        if (err) {
          bot.reply(msg.channel, "I've failed to set NSFW flag!");
        }
        if (allow === "on") {
          bot.sendMessage(msg.channel, "NSFW commands are now allowed for " + msg.channel);
        } else if (allow === "off") {
          bot.sendMessage(msg.channel, "NSFW commands are now disallowed for " + msg.channel);
        } else {
          bot.reply(msg.channel, "I've failed to set NSFW flag!");
        }
      });
    }
  }
};

Commands.hello = {
  name: "hello",
  help: "I'll respond to you with hello",
  level: 0,
  fn: function(bot, msg) {
    bot.sendMessage(msg.channel, "Good Day " + msg.sender + "! I'm " + bot.user.username + ", type **\\\!help\** for a list of my commands!");
  }
};

Commands["server-info"] = {
  name: "server-info",
  help: "I'll tell you some information about the server and the channel you're currently in.",
  level: 0,
  fn: function(bot, msg, suffix) {
    // if we're not in a PM, return some info about the channel
    if (msg.channel.server) {
      var msgArray = [];
      msgArray.push("You are currently in " + msg.channel + " (id: " + msg.channel.id + ")");
      msgArray.push("on server **" + msg.channel.server.name + "** (id: " + msg.channel.server.id + ") (region: " + msg.channel.server.region + ")");
      msgArray.push("owned by " + msg.channel.server.owner + " (id: " + msg.channel.server.owner.id + ")");
      if (msg.channel.topic) {
        msgArray.push("The current topic is: " + msg.channel.topic);
      }
      bot.sendMessage(msg, msgArray);
    } else {
      bot.sendMessage(msg, "You can't do that in a DM, dummy!.");
    }
  }
};

Commands.invite = {
        name: "invite",
        description: "Creates an invite link that valid for 5 minutes.",
        level: 0,
        fn: function(bot,msg) {
            var opt = {};
            opt.maxAge = 300;
            opt.maxUses = 1;
            opt.temporary = false;
            opt.xkcd = true;
            bot.createInvite(msg.channel, opt, function(error, invite) {
                bot.sendMessage(msg, 'This invite can only be used once and is valid for 5 minutes. https://discord.gg/' + invite.code);
            });
        }
    };

Commands["join-server"] = {
  name: "join-server",
  help: "I'll join the server you've requested me to join, as long as the invite is valid and I'm not banned of already in the requested server.",
  usage: "<bot-username> <instant-invite>",
  level: 0,
  fn: function(bot, msg, suffix) {
    suffix = suffix.split(" ");
    if (suffix[0] === bot.user.username) {
      Logger.log("debug", bot.joinServer(suffix[1], function(error, server) {
        Logger.log("debug", "callback: " + arguments);
        if (error || !server) {
          Logger.warn("Failed to join a server: " + error);
          bot.sendMessage(msg.channel, "Something went wrong, try again.");
        } else {
          var msgArray = [];
          msgArray.push("Yo! I'm **" + bot.user.username + "**, " + msg.author + " invited me to this server.");
          msgArray.push("If I'm intended to be in this server, you may use **" + ConfigFile.bot_settings.cmd_prefix + "help** to see what I can do!");
          msgArray.push("If you don't want me here, you may use **" + ConfigFile.bot_settings.cmd_prefix + "leave** to ask me to leave.");
          msgArray.push("By the way, to give " + server.owner + " administrative permissions over me, use **" + ConfigFile.bot_settings.cmd_prefix + "setowner**");
          bot.sendMessage(server.defaultChannel, msgArray);
          msgArray = [];
          msgArray.push("Hey " + server.owner.username + ", I've joined a server in which you're the founder.");
          msgArray.push("I'm " + bot.user.username + " by the way, a Discord bot, meaning that all of the things I do are mostly automated.");
          msgArray.push("If you are not keen on having me in your server, you may use `" + ConfigFile.bot_settings.cmd_prefix + "leave` in the server I'm not welcome in.");
          msgArray.push("If you do want me, use `" + ConfigFile.bot_settings.cmd_prefix + "help` to see what I can do.");
          bot.sendMessage(server.owner, msgArray);
          bot.sendMessage(msg.channel, "I've successfully joined **" + server.name + "**");
        }
      }));
    } else {
      Logger.log("debug", "Ignoring join command meant for another bot.");
    }
  }
};

Commands['check-voice'] = {
  name: "check-voice",
  help: "I'll check if I'm avalible to stream music right now.",
  level: 0,
  fn: function(bot, msg) {
    DJ.checkIfAvalible(bot, msg);
  }
};

Commands.meme = {
  name: "meme",
  help: "I'll create a meme with your suffixes!",
  usage: '<memetype> "<Upper line>" "<Bottom line>" **Quotes are important!**',
  level: 0,
  fn: function(bot, msg, suffix) {
    var tags = msg.content.split('"');
    var memetype = tags[0].split(" ")[1];
    var meme = require("./memes.json");
    //bot.sendMessage(msg.channel,tags);
    var Imgflipper = require("imgflipper");
    var imgflipper = new Imgflipper(ConfigFile.imgflip.username, ConfigFile.imgflip.password);
    imgflipper.generateMeme(meme[memetype], tags[1] ? tags[1] : "", tags[3] ? tags[3] : "", function(err, image) {
      //CmdErrorLog.log("debug", arguments);
      bot.sendMessage(msg.channel, image);
      bot.deleteMessage(msg);
      if (!msg.channel.server) {
        return;
      }
      var bot_permissions = msg.channel.permissionsOf(bot.user);
      if (bot_permissions.hasPermission("manageMessages")) {
        return;
      } else {
        bot.sendMessage(msg.channel, "*This works best when I have the permission to delete messages!*");
      }
    });
  }
};

Commands.boobs = {
  name: "boobs",
  help: "Fetches a random boob pic.",
  level: 0,
  nsfw: true,
  fn: function(bot, msg) {
          var number = Math.floor(Math.random() * 9999) + 1001;
                bot.sendMessage(msg.channel,"http://media.oboobs.ru/boobs_preview/0" + number +".jpg" );
                bot.deleteMessage(msg);
            }
    };
Commands.butts = {
  name: "butts",
  help: "Fetches a random butt pic.",
  level: 0,
  nsfw: true,
    fn: function(bot, msg) {
          var number = Math.floor(Math.random() * 3200) + 1000;
                bot.sendMessage(msg.channel,"http://media.obutts.ru/butts_preview/0" + number +".jpg" );
                bot.deleteMessage(msg);
            }
    };

Commands.rule34 = {
  name: "rule34",
  help: "Rule#34 : If it exists there is porn of it. If not, start uploading.",
  level: 0,
  nsfw: true,
  fn: function(bot, msg, suffix) {
    var request = require('request');
    var xml2js = require('xml2js');
    var url = "http://rule34.xxx/index.php?page=dapi&s=post&limit=1&q=index&tags=" + suffix;
    request(url, function(error, response, body) {
      if (!error && body.length > 74 && response.statusCode == 200) {
        xml2js.parseString(body, function(err, result) {
          var util = require('util');
          var fuckme = util.inspect(result.posts, false, null).split("'");
          if (fuckme[13].substr(0, 5) != "http:") {
            var httpstring = 'http:';
            fuckme = httpstring.concat(fuckme[13]);
            bot.sendMessage(msg.channel, fuckme);
          } else {
            bot.sendMessage(msg.channel, fuckme[13]);
            bot.deleteMessage(msg);
          }
        });
      } else {
        Logger.error("Got an error: ", error, ", status code: ", response.statusCode);
        bot.reply(msg, "sorry, no image found.");
      }
    });
  }
};

Commands.madame = {
  name: "madame",
  help: "Display's the Madame of the day.",
  level: 0,
  nsfw: true,
	fn: function(bot,msg) {
	request('http://ditesbonjouralamadame.tumblr.com/random', function(err, res, body) {
		if (err)
			return console.log(err);

		var url = $(body).find('.photo.post').first().find('img').first().attr('src');
		bot.sendMessage(msg, 'Madam of the day : ' + url);
		bot.deleteMessage(msg);
	});
}
};

Commands.pr0n = {
  level: 0,
  nsfw: true,
	fn: function(bot,msg) {
	request('http://xxxsexxx.tumblr.com/random', function(err, res, body) {
		if (err)
			return console.log(err);

		var url = $(body).find('.photo').first().find('img').first().attr('src');
		bot.sendMessage(msg, url);
		bot.deleteMessage(msg);
	});
}
};

Commands.gif = {
  name: "gif",
  help: "I will search Giphy for a gif matching your tags.",
  usage: "<image tags>",
  level: 0,
  fn: function(bot, msg, suffix) {
    var tags = suffix.split(" ");
    Giphy.get_gif(tags, function(id) {
      if (typeof id !== "undefined") {
        bot.sendMessage(msg.channel, "http://media.giphy.com/media/" + id + "/giphy.gif [Tags: " + (tags ? tags : "Random GIF") + "]");
        bot.deleteMessage(msg);
      } else {
        bot.sendMessage(msg.channel, "Invalid tags, try something different. For example, something that exists [Tags: " + (tags ? tags : "Random GIF") + "]");
      }
    });
  }
};

Commands.stroke = {
  name: "stroke",
  help: "I'll stroke someones ego, how nice of me.",
  usage: "[First name][, [Last name]]",
  level: 0,
  fn: function(bot, msg, suffix) {
    var name;
    if (suffix) {
      name = suffix.split(" ");
      if (name.length === 1) {
        name = ["", name];
      }
    } else {
      name = ["Perpetu", "Cake"];
    }
    var request = require('request');
    request('http://api.icndb.com/jokes/random?escape=javascript&firstName=' + name[0] + '&lastName=' + name[1], function(error, response, body) {
      if (!error && response.statusCode == 200) {
        var joke = JSON.parse(body);
        bot.sendMessage(msg.channel, joke.value.joke);
        bot.deleteMessage(msg);
      } else {
        Logger.log("warn", "Got an error: ", error, ", status code: ", response.statusCode);
      }
    });
  }
};

Commands.advice = {
  name: "advice",
  help: "I'll give you some great advice, I'm just too kind.",
  level: 0,
  fn: function(bot, msg, suffix) {
    var request = require('request');
    request('http://api.adviceslip.com/advice', function(error, response, body) {
      if (!error && response.statusCode == 200) {
        var advice = JSON.parse(body);
        bot.sendMessage(msg.channel, advice.slip.advice);
        bot.deleteMessage(msg);
      } else {
        Logger.log("warn", "Got an error: ", error, ", status code: ", response.statusCode);
      }
    });
  }
};

Commands.decide = {
  name: "decide",
  help: "Ever wanted a gif displaying your (dis)agreement? Then look no further!",
  usage: "optional: [force yes/no/maybe]",
  level: 0,
  fn: function(bot, msg, suffix) {
    var request = require('request');
    request('http://yesno.wtf/api/?force=' + suffix, function(error, response, body) {
      if (!error && response.statusCode == 200) {
        var yesNo = JSON.parse(body);
        bot.sendMessage(msg.channel, msg.sender + " " + yesNo.image);
      } else {
        Logger.log("warn", "Got an error: ", error, ", status code: ", response.statusCode);
      }
    });
  }
};

Commands.reverse = {
  name: "reverse",
  help: " I will repeat what you say in reverse.",
  level: 0,
  fn: function(bot, message, params, errorCallback) {

        // build a RegEx for mentions and channels
        var mentionRegEx = (/<@(\d{17})>/);
        var channelRegEx = (/<#(\d{17})>/);

        // cycle params for mentions and channels
        for (var i = 0; i < params.length; i++) {
            if (params[i].match(mentionRegEx)) {

                // grab username and convert param into a string
                var userId = mentionRegEx.exec(params[i])[1];
                params[i] = "@" + bot.users.get("id", userId).username;
            }
            if (params[i].match(channelRegEx)) {

                // grab channel name and convert param into a string
                var channelId = channelRegEx.exec(params[i])[1];
                params[i] = "#" + bot.channels.get("id", channelId).name;
            }
        }

        // reverse it and send it.
        bot.sendMessage(message, params.split("").reverse().join("")).catch(errorCallback);
    }
    };

Commands.urban = {
  name: "urban",
  help: "Get results from the Urban Dictionary",
  usage: "[string]",
  level: 0,
  fn: function(bot, msg, suffix) {
    var request = require('request');
    request('http://api.urbandictionary.com/v0/define?term=' + suffix, function(error, response, body) {
      if (!error && response.statusCode == 200) {
        var uD = JSON.parse(body);
        if (uD.result_type !== "no_results") {
          bot.sendMessage(msg.channel, suffix + ": " + uD.list[0].definition + ' "' + uD.list[0].example + '"');
          bot.deleteMessage(msg);
        } else {
          bot.sendMessage(msg.channel, suffix + ": This is so screwed up, even Urban Dictionary doesn't have it in it's database");
        }
      } else {
        Logger.log("warn", "Got an error: ", error, ", status code: ", response.statusCode);
      }
    });
  }
};

Commands.fact = {
  name: "fact",
  help: "I'll give you some interresting facts!",
  level: 0,
  fn: function(bot, msg, suffix) {
    var request = require('request');
    var xml2js = require('xml2js');
    request("http://www.fayd.org/api/fact.xml", function(error, response, body) {
      if (!error && response.statusCode == 200) {
        //Logger.log("debug", body)
        xml2js.parseString(body, function(err, result) {
          bot.sendMessage(msg.channel, result.facts.fact[0]);
          bot.deleteMessage(msg);
        });
      } else {
        Logger.log("warn", "Got an error: ", error, ", status code: ", response.statusCode);
      }
    });
  }
};

Commands.numberfacts = {
  name: "numberfacts",
  help: "I'll post a random fact about numbers.",
  level: 0,
  fn: function(bot, msg, suffix) {
			var number = "random";
			if (suffix && /^\d+$/.test(suffix)) { number = suffix; }
			request('http://numbersapi.com/'+number+'/trivia?json', function (error, response, body) {
				if (error) { bot.sendMessage(msg, "Error: "+error, function (erro, wMessage) { bot.deleteMessage(wMessage, {"wait": 10000}); }); }
				if (response.statusCode != 200) { bot.sendMessage(msg, "Got status code "+response.statusCode, function (erro, wMessage) { bot.deleteMessage(wMessage, {"wait": 10000}); }); }
				if (!error && response.statusCode == 200) {
					body = JSON.parse(body);
					bot.sendMessage(msg, body.text);
					bot.deleteMessage(msg);
				}
			});
		}
};

Commands.xkcd = {
  name: "xkcd",
  help: "I'll get a XKCD comic for you, you can define a comic number and I'll fetch that one.",
  usage: "[current, or comic number]",
  level: 0,
  fn: function(bot, msg, suffix) {
    var request = require('request');
    request('http://xkcd.com/info.0.json', function(error, response, body) {
      if (!error && response.statusCode == 200) {
        var xkcdInfo = JSON.parse(body);
        if (suffix) {
          var isnum = /^\d+$/.test(suffix);
          if (isnum) {
            if ([suffix] < xkcdInfo.num) {
              request('http://xkcd.com/' + suffix + '/info.0.json', function(error, response, body) {
                if (!error && response.statusCode == 200) {
                  xkcdInfo = JSON.parse(body);
                  bot.sendMessage(msg.channel, xkcdInfo.img);
                  bot.deleteMessage(msg);
                } else {
                  Logger.log("warn", "Got an error: ", error, ", status code: ", response.statusCode);
                }
              });
            } else {
              bot.sendMessage(msg.channel, "There are only " + xkcdInfo.num + " xkcd comics!");
            }
          } else {
            bot.sendMessage(msg.channel, xkcdInfo.img);
          }
        } else {
          var xkcdRandom = Math.floor(Math.random() * (xkcdInfo.num - 1)) + 1;
          request('http://xkcd.com/' + xkcdRandom + '/info.0.json', function(error, response, body) {
            if (!error && response.statusCode == 200) {
              xkcdInfo = JSON.parse(body);
              bot.sendMessage(msg.channel, xkcdInfo.img);
            } else {
              Logger.log("warn", "Got an error: ", error, ", status code: ", response.statusCode);
            }
          });
        }

      } else {
        Logger.log("warn", "Got an error: ", error, ", status code: ", response.statusCode);
      }
    });
  }
};

/*Commands.csgoprice = {
  name: "csgoprice",
  help: "I'll give you the price of a CS:GO skin.",
  usage: '[weapon "AK-47"] [skin "Vulcan"] [[wear "Factory New"] [stattrak "(boolean)"]] Quotes are important!',
  level: 0,
  fn: function(bot, msg, suffix) {
    skinInfo = suffix.split('"');
    var csgomarket = require('csgo-market');
    csgomarket.getSinglePrice(skinInfo[1], skinInfo[3], skinInfo[5], skinInfo[7], function(err, skinData) {
      if (err) {
        Logger.log('error', err);
        bot.sendMessage(msg.channel, "That skin is so super secret rare, it doesn't even exist!");
      } else {
        if (skinData.success === true) {
          if (skinData.stattrak) {
            skinData.stattrak = "Stattrak";
          } else {
            skinData.stattrak = "";
          }
          var msgArray = ["Weapon: " + skinData.wep + " " + skinData.skin + " " + skinData.wear + " " + skinData.stattrak, "Lowest Price: " + skinData.lowest_price, "Number Available: " + skinData.volume, "Median Price: " + skinData.median_price, ];
          bot.sendMessage(msg.channel, msgArray);
        }
      }
    });
  }
};*/

Commands.roll = {
  name: "roll",
  help: "returns a random number between 1 and max value. If no max is specified it is 10",
  level: 0,
  fn: function(bot,msg,suffix) {
			var max = 10;
			if(suffix) max = suffix;
			var val = Math.floor(Math.random() * max) + 1;
			bot.sendMessage(msg.channel,msg.author + " 🎲 rolled a " + val);
			bot.deleteMessage(msg);
		}
	};

Commands.fancyinsult = {
  name: "fancyinsult",
  help: "I'll insult your friends, in style.",
  level: 0,
  fn: function(bot, msg, suffix) {
    var request = require('request');
    request('http://quandyfactory.com/insult/json/', function(error, response, body) {
      if (!error && response.statusCode == 200) {
        var fancyinsult = JSON.parse(body);
        if (suffix === "") {
          bot.sendMessage(msg.channel, fancyinsult.insult);
          bot.deleteMessage(msg);
        } else {
          bot.sendMessage(msg.channel, suffix + ", " + fancyinsult.insult);
        }
      } else {
        Logger.log("warn", "Got an error: ", error, ", status code: ", response.statusCode);
      }
    });
  }
};

Commands.imdb = {
  name: "imdb",
  help: "I'll search through IMDb for a movie matching your given tags, and post my finds in the channel.",
  usage: "[title]",
  level: 0,
  fn: function(bot, msg, suffix) {
    if (suffix) {
      var request = require('request');
      request('http://api.myapifilms.com/imdb/title?format=json&title=' + suffix + '&token=' + ConfigFile.api_keys.myapifilms_token, function(error, response, body) {
        if (!error && response.statusCode == 200) {
          var imdbInfo = JSON.parse(body);
          imdbInfo = imdbInfo.data.movies[0];
          if (imdbInfo) {
            //Date snatching
            var y = imdbInfo.releaseDate.substr(0, 4),
              m = imdbInfo.releaseDate.substr(4, 2),
              d = imdbInfo.releaseDate.substr(6, 2);
            var msgArray = [imdbInfo.title, imdbInfo.plot, " ", "Released on: " + m + "/" + d + "/" + y, "Rated: " + imdbInfo.rated + imdbInfo.rating + "/10"];
            var sendArray = [imdbInfo.urlIMDB, msgArray];
            for (var i = 0; i < sendArray.length; i++) {
              bot.sendMessage(msg.channel, sendArray[i]);
              bot.deleteMessage(msg);
            }
          } else {
            bot.sendMessage(msg.channel, "Search for " + suffix + " failed!");
          }
        } else {
          Logger.log("warn", "Got an error: ", error, ", status code: ", response.statusCode);
        }
      });
    } else {
      bot.sendMessage(msg.channel, "Usage: !imdb [title]");
    }
  }
};

Commands["8ball"] = {
  name: "8ball",
  help: "I'll function as an magic 8 ball for a bit and anwser all of your questions! (So long as you enter the questions as suffixes.)",
  usage: "<question>",
  level: 0,
  fn: function(bot, msg, suffix) {
    var request = require('request');
    request('https://8ball.delegator.com/magic/JSON/0', function(error, response, body) {
      if (!error && response.statusCode == 200) {
        var eightBall = JSON.parse(body);
        bot.sendMessage(msg.channel, eightBall.magic.answer + ", " + msg.sender);
      } else {
        Logger.log("warn", "Got an error: ", error, ", status code: ", response.statusCode);
      }
    });
  }
};

Commands.catfacts = {
  name: "catfacts",
  help: "I'll give you some interresting facts about cats!",
  level: 0,
  fn: function(bot, msg, suffix) {
    var request = require('request');
    request('http://catfacts-api.appspot.com/api/facts', function(error, response, body) {
      if (!error && response.statusCode == 200) {
        var catFact = JSON.parse(body);
        bot.sendMessage(msg.channel, catFact.facts[0]);
        bot.deleteMessage(msg);
      } else {
        Logger.log("warn", "Got an error: ", error, ", status code: ", response.statusCode);
      }
    });
  }
};

Commands.help = {
  level: 0,
 		fn: function(bot,msg) {
 			bot.reply(msg, "Very good Sir! 📧 I have mailed you list of my commands!")
 			bot.sendFile(msg.author, "./images/jarvislogosmall.png")
 			function callback(a){
 			return function(){
 			bot.sendMessage(msg.author, help);
 			bot.deleteMessage(msg);
    }
}
 			setTimeout(callback(help), 2000);
 		}
 	};

Commands.info = {
  name: "info",
  help: "You're looking at it right now.",
  level: 0,
  fn: function(bot, msg, suffix) {
    var msgArray = [];
    var commandnames = []; // Build a array of names from commands.
    if (!suffix) {
      for (index in Commands) {
        commandnames.push(Commands[index].name);
      }
      msgArray.push("These are the currently avalible commands, use `" + ConfigFile.bot_settings.cmd_prefix + "info <command_name>` to learn more about a specific command.");
      msgArray.push("");
      msgArray.push(commandnames.join(", "));
      msgArray.push("");
      msgArray.push("If you have any questions, or if you don't get something, contact <@70831962033491968>");
      if (ConfigFile.bot_settings.help_mode === "private") {
        bot.sendMessage(msg.author, msgArray);
        bot.deleteMessage(msg);
        Logger.debug("Send help via DM.");
        if (msg.channel.server) {
          bot.sendMessage(msg.channel, "I have mailed you the info on my commands!");
        }
      } else if (ConfigFile.bot_settings.help_mode === "channel") {
        bot.sendMessage(msg.channel, msgArray);
        Logger.debug("Send help to channel.");
      } else {
        Logger.error("Config File error! Help mode is incorrectly defined!");
        bot.sendMessage(msg.channel, "Sorry, my owner didn't configure me correctly!");
      }
    }
    if (suffix) {
      if (Commands[suffix]) { // Look if suffix corresponds to a command
        var commando = Commands[suffix]; // Make a varialbe for easier calls
        msgArray = []; // Build another message array
        msgArray.push("**Command:** `" + commando.name + "`"); // Push the name of the command to the array
        msgArray.push(""); // Leave a whiteline for readability
        if (commando.hasOwnProperty("usage")) { // Push special message if command needs a suffix.
          msgArray.push("**Usage:** `" + ConfigFile.bot_settings.cmd_prefix + commando.name + " " + commando.usage + "`");
        } else {
          msgArray.push("**Usage:** `" + ConfigFile.bot_settings.cmd_prefix + commando.name + "`");
        }
        msgArray.push("**Description:** " + commando.help); // Push the extendedhelp to the array.
        if (commando.hasOwnProperty("nsfw")) { // Push special message if command is restricted.
          msgArray.push("**This command is NSFW, so it's restricted to certain channels and DM's.**");
        }
        if (commando.hasOwnProperty("timeout")) { // Push special message if command has a cooldown
          msgArray.push("**This command has a cooldown of " + commando.timeout + " seconds.**");
        }
        msgArray.push("**Needed access level:** " + commando.level); // Push the needed access level to the array
        if (suffix == "meme") { // If command requested is meme, print avalible meme's
          msgArray.push("");
          var str = "**Currently available memes:\n**";
          var meme = require("./memes.json");
          for (var m in meme) {
            str += m + ", ";
          }
          msgArray.push(str);
        }
        if (ConfigFile.bot_settings.help_mode === "private") {
          bot.sendMessage(msg.author, msgArray);
          Logger.debug("Send suffix help via DM.");
        } else if (ConfigFile.bot_settings.help_mode === "channel") {
          bot.sendMessage(msg.channel, msgArray);
          Logger.debug("Send suffix help to channel.");
        } else {
          Logger.error("Config File error! Help mode is incorrectly defined!");
          bot.sendMessage(msg.channel, "Sorry, my owner didn't configure me correctly!");
        }
      } else {
        bot.sendMessage(msg.channel, "There is no **" + suffix + "** command!");
      }
    }
  }
};

Commands.superpower = {
        name: "superpower",
        help: "I will give you or a mentioned user a superpower",
        level: 0,
        fn: function(bot, msg, suffix) {
            var request = require("request");
            var id = "48473";
            var powerlisting = "http://www.wikia.com/api/v1/Wikis/Details?ids=" + id;
            var power;
            var capabilities;
            request("http://powerlisting.wikia.com/wiki/Special:Random", function(err, response, body) {
                if (err) {
                    throw err;}
                if (response.statusCode === 200) {
                    var str1 = response.request.uri.path;
                    var str2 = str1.replace("\/wiki\/", "");
                    request("http://www.powerlisting.wikia.com/api/v1/Articles/List?expand=1&limit=1&offset=" + str2, function(err, response, body) {
                        if (err) {
                            throw err;}
                        if (response.statusCode === 200) {
                            var wikijson = JSON.parse(body);
                            var id = wikijson.items[0].id;
                            power = wikijson.items[0].title;
                            request("http://www.powerlisting.wikia.com/api/v1/Articles/AsSimpleJson?id=" + id, function(err, response, body) {
                                if (err) {
                                    throw err;}
                                if (response.statusCode === 200) {
                                    var wikijson = JSON.parse(body).sections;
                                    for (var i = 0; i < wikijson.length; i++) {
                                        if (wikijson[i].title === "Capabilities" || wikijson[i].title === "Capability") {
                                            capabilities = wikijson[i].content[0].text;}}
                                    if (!suffix) {
                                        var msgArray = [];
                                        msgArray.push(msg.sender + "'s superpower is: " + "**" + power + "!" + "**");
                                        msgArray.push(capabilities);
                                        bot.sendMessage(msg.channel, msgArray);
                                        return;}
                                    else if (suffix) {
                                        if (msg.mentions.length === 0) {
                                            bot.sendMessage(msg.channel, "⚠ Please mention the user you want to give a superpower to!~");
                                            return;}
                                        msg.mentions.map(function(user) {
                                            var msgArray = [];
                                            msgArray.push(user + "'s superpower is: " + "**" + power + "!" + "**");
                                            console.log(user.username + "'s superpower is: " + str2);
                                            msgArray.push(capabilities);
                                            bot.sendMessage(msg.channel, msgArray);
                                            bot.deleteMessage(msg);
                                            return;
                                        });
                                    }
                                }
                            });
                        }
                    });
                }
            });
        }
    };
    
Commands.rps = {
        name: "rps",
        help: "Play Rock, Paper, Scissors against me.",
        level: 0,
        fn: function(bot,msg,suffix) {
        var thrown = msg.content.replace("!rps ", "").replace("!rps", "");
        if (thrown.length > 0) {
            var randomNumber = Math.floor((Math.random() * 3) + 1);

            switch (thrown) {
                case "rock":
                    if (randomNumber == 1)
                        bot.reply(msg, "I threw :moyai:\nIt's a tie!")
                    if (randomNumber == 2)
                        bot.reply(msg, "I threw :page_facing_up:\nI win!");
                    if (randomNumber == 3)
                        bot.reply(msg, "I threw :scissors:\nI lose.");
                    break;
                case "paper":
                    if (randomNumber == 1)
                        bot.reply(msg, "I threw :moyai:\nI lose.")
                    if (randomNumber == 2)
                        bot.reply(msg, "I threw :page_facing_up:\nIt's a tie!");
                    if (randomNumber == 3)
                        bot.reply(msg, "I threw :scissors:\nI win!");
                    break;

                case "scissors":
                    if (randomNumber == 1)
                        bot.reply(msg, "I threw :moyai:\nI win!")
                    if (randomNumber == 2)
                        bot.reply(msg, "I threw :page_facing_up:\nI lose.");
                    if (randomNumber == 3)
                        bot.reply(msg, "I threw :scissors:\nIt's a tie!");
                    break;


            }
        } else {
            bot.reply(msg, "To play use either rock/paper or scissors")
            }
        }
    };

Commands.cards = {
        name: "cards",
        help: "I will display a random set of cards (WIP)",
        usage: "<number of cards> (default=5)",
        level: 0,
        fn: function(bot,msg,suffix) {
        var cards = ["A♠", "2♠", "3♠", "4♠", "5♠", "6♠", "7♠", "8♠", "9♠", "10♠", "J♠", "Q♠", "K♠", "A♥", "2♥", "3♥", "4♥", "5♥", "6♥", "7♥", "8♥", "9♥", "10♥", "J♥", "Q♥", 
        "K♥", "A♦", "2♦", "3♦", "4♦", "5♦", "6♦", "7♦", "8♦", "9♦", "10♦", "J♦", "Q♦", "K♦", "A♣", "2♣", "3♣", "4♣", "5♣", "6♣", "7♣", "8♣", "9♣", "10♣", "J♣", "Q♣", "K♣"];    

		var number = parseInt(msg.content.replace("!cards", "").replace(" ", ""));
		if (isNaN(number)){
			number = 5;
		}
		else if (number > 52){
			number = 52;
		}
		var temp_cards = cards.slice();
		temp_cards = shuffle(temp_cards);
		
		var held_cards = temp_cards.splice(0, number);
		
		bot.sendMessage(msg, held_cards.toString());
		bot.deleteMessage(msg);
        }
	};
	function shuffle(array) {
    var currentIndex = array.length,
        temporaryValue, randomIndex;

    // While there remain elements to shuffle...
    while (0 !== currentIndex) {

        // Pick a remaining element...
        randomIndex = Math.floor(Math.random() * currentIndex);
        currentIndex -= 1;

        // And swap it with the current element.
        temporaryValue = array[currentIndex];
        array[currentIndex] = array[randomIndex];
        array[randomIndex] = temporaryValue;
    }

    return array;
};

Commands.coinflip = {
    name: "coinflip",
		help: "I will flip a coin, what will it be?",
		level: 0,
		fn: function(bot, msg, suffix) {
			var side = Math.floor(Math.random() * (2));
			if (side == 0) { bot.reply(msg, "*flips a coin and...* **Heads**");
			} else { bot.reply(msg, "*flips a coin and...* **Tails**");
			bot.deleteMessage(msg);
			}
		}
	};

Commands.cyanide = {
  level: 0,
  fn: function(bot,msg) {
      var r = request.get('http://explosm.net/comics/random', function (err, res, body) {
        if (err) {
          console.log(err);
        }
        bot.sendMessage(msg.channel, r.uri.href);
        bot.deleteMessage(msg);
        });
      }
};
	
Commands.game = {
        name: "game",
        usage: "(name of game)",
        help: "mentions everyone if they want to play (name of game)",
        level: 0,
        fn: function(bot, msg, suffix) {
            var game = game_abbreviations[suffix];
            if (!game) {
              game = suffix;
            }
      bot.sendMessage(msg, "@everyone, " + msg.sender + " would like to know if anyone is up for **" + game +"**");
      Logger.log("debug", "Sent game invites for " + game);
    }
  };
  
Commands.gems = {
  name: "gems",
  help: "Will show the current Gold-Gem exchange rate",
  usage: "<gem amount>",
  level: 0,
  fn: function(bot, msg, suffix) {
    request("http://www.gw2spidy.com/api/v0.9/json/gem-price", function(error, response, body) {
      if (!error && response.statusCode == 200) {
        var gw2api = JSON.parse(body);
        var gems = (gw2api.result.gold_to_gem/10000/100*suffix);
        bot.reply(msg, suffix+":gem:"+" will cost you **"+gems+"** gold");
        bot.deleteMessage(msg);
      }
      });
  }
};

Commands.dailies = {
  name: "dailies",
  help: "Will display today's Guild Wars 2 dailies",
  level: 0,
  fn: function(bot, msg) {
    var daily = require("./gw2daily.json");
    var date = new Date();
    request("https://api.guildwars2.com/v2/achievements/daily", function(error, response, body) {
      if (!error && response.statusCode == 200) {
        var dailyapi = JSON.parse(body);
    var pve0 = (dailyapi.pve[0].id), pve1 = (dailyapi.pve[1].id), pve2 = (dailyapi.pve[2].id), pve3 = (dailyapi.pve[3].id);
    var pvp0 = (dailyapi.pvp[0].id), pvp1 = (dailyapi.pvp[1].id), pvp2 = (dailyapi.pvp[2].id), pvp3 = (dailyapi.pvp[3].id);
    var wvw0 = (dailyapi.wvw[0].id), wvw1 = (dailyapi.wvw[1].id), wvw2 = (dailyapi.wvw[2].id), wvw3 = (dailyapi.wvw[3].id);
    var level = (dailyapi.pve[1].level.max == 80);
    var str = "**🐲 Guild Wars 2 Daily ("+date.toDateString()+")**\n\n"+"**PvE:** \n" + daily[pve0] + " | " + daily[pve1] + "\n" + daily[pve2] + " | " + daily[pve3] + "\n\n"
				    + "**PvP:** \n" + daily[pvp0] + " | " + daily[pvp1] + " | " + daily[pvp2] + " | " + daily[pvp3] + "\n\n"
				    + "**WvW:** \n" + daily[wvw0] + " | " + daily[wvw1] + " | " + daily[wvw2] + "\n" + daily[wvw3];
				    bot.sendMessage(msg, str);
				    bot.deleteMessage(msg);
      }
      });
  }
};

Commands.ps2 = {
  name: "ps2",
  help: "Display's stats for your PS2 character",
  level: 0,
  fn: function(bot, msg, suffix) {
    var ps2api = "http://census.daybreakgames.com/s:sikksens/json/get/ps2:v2/character/?name.first_lower="+suffix;
    request(ps2api, function(error, response, html) {
      if (!error && response.statusCode == 200) {
        var ps2 = JSON.parse(html);
        var faction = {
          "1" : "Vanu Sovereignty",
          "2" : "New Conglomerate",
          "3" : "Terran Republic"
        }
     var stats = [];
     stats.push("***Planetside 2 Stats:***")
     stats.push("----------------------")
     stats.push("**Name: **" + ps2.character_list[0].name.first)
     stats.push("**ID: **" + ps2.character_list[0].character_id)
     stats.push("**Faction: **" + faction[ps2.character_list[0].faction_id])
     stats.push("**Battle Rank: **" + ps2.character_list[0].battle_rank.value+" ("+ps2.character_list[0].battle_rank.percent_to_next+"% til next rank)")
     stats.push("**Certs: **" + ps2.character_list[0].certs.available_points)
     stats.push("**Playtime: **" + Math.round(ps2.character_list[0].times.minutes_played / 60)+" hours")
     stats.push("**Last Login: **" + ps2.character_list[0].times.last_login_date)
     bot.sendMessage(msg, stats);
    }
    });
  }
};

exports.Commands = Commands;
